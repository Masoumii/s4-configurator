        /* Class: Configurator */
        function Configurator(){
              
            /* Calculates the sum of "Basisinrichting" */
            this.calculateSumOfBasis = function(){
                let defaultSum = 0;
                /* For each checkbox input of the default items */
		$(".basis-cb").each(function (index, el) {
                    if ($(this).val() !== "") { // Get the sum only if value is not empty 
                            defaultSum += $(this).val() * $(this).data("price"); // Calculate price: (Price of item multiplied by the amount of items )
			}
		});
                /* Set default total sum into localStorage */
                localStorage.setItem('sum_default', defaultSum); // Save result to local storage
		$(".basis-amount").html(defaultSum);
            },
                    
            /* Calculates the sum of "Aanvullende investeringen" */        
            this.calculateSumOfAanvullend = function(){
                let additionalSum = 0;
                    $(".aanvullend-cb").each(function () {
                        if ($(this).val() !== "") { // Get the sum only if value is not empty 
                            additionalSum += $(this).val() * $(this).data("price"); // Calculate price: (Price of item multiplied by the amount of items )
			}
		});
		localStorage.setItem('sum_additional', additionalSum); // Save result to local storage
		$('.aanvullend-amount').html(additionalSum); // Show sum of additional items 
            },
                 
                    
             /* Calculates the monthly sum of "Aanvullende investeringen" */        
            this.calculateMonthlySumOfAanvullend = function(){
                let additionalMonthlySum = 0;
                    $(".aanvullend-cb").each(function () {
                        if ($(this).val() !== "") { // Get the sum only if value is not empty 
                            additionalMonthlySum += $(this).val() * $(this).data("monthly-price"); // Calculate price: (Price of item multiplied by the amount of items )
			}
		});
		localStorage.setItem('sum_monthly_additional', additionalMonthlySum); // Save result to local storage
		$('.aanvullend-monthly-amount').html(additionalMonthlySum); // Show sum of additional items 
            },        
            
            
            /* Calculates the sum of "Modules" */        
            this.calculateSumOfModules = function(){
                let moduleSum = 0;
		$(".modules-cb").each(function () {
			if ($(this).val() !== "") { // Get the sum only if value is not empty 
                            moduleSum += $(this).val() * $(this).data("price"); // Calculate price: (Price of item multiplied by the amount of items )
			}
		});
		localStorage.setItem('sum_modules', moduleSum); // Save result to local storage
		$('.modules-amount').html(moduleSum); // Show sum of additional items 
            },
                
                    
            /* Calculates the sum of "Modules" */        
            this.calculateMonthlySumOfModules = function(){
                let moduleMonthlySum = 0;
		$(".modules-cb").each(function () {
			if ($(this).val() !== "") { // Get the sum only if value is not empty 
                            moduleMonthlySum += $(this).val() * $(this).data("monthly-price"); // Calculate price: (Price of item multiplied by the amount of items )
			}
		});
		localStorage.setItem('sum_monthly_modules', moduleMonthlySum); // Save result to local storage
		$('.modules-amount-monthly').html(moduleMonthlySum); // Show sum of additional items 
            },      
                    
            
            /* Calculates the total sum of all chosen items */
            this.calculateSumOfAll = function(){
                
                let sumDefaultItems = localStorage.getItem('sum_default'); // Retrieve sum of default items 
		let sumAdditionalItems = localStorage.getItem('sum_additional'); // Retrieve sum of additional items
                
                let sumModulesItems = localStorage.getItem('sum_modules'); // Retrieve sum of modules items
		let totalSum = parseInt(sumDefaultItems) + parseInt(sumAdditionalItems) + parseInt(sumModulesItems); // Calculate total sum of default, additonal items & modules items
		localStorage.setItem('sum_total', totalSum); // Save the total sum to local storage
		$('.total-amount').html(totalSum); // Show total sum
            },
                    
                    
            /* Calculates the total sum of all chosen items (Edit blueprint) */
            this.editCalculateSumOfAll = function(){
                
                let totalEditSum = 0;
		
                $(".edit-blauwdruk-item:input").each(function (){
			if ($(this).val() !== "") { // Get the sum only if value is not empty 
                            totalEditSum += $(this).val() * $(this).data("price"); // Calculate price: (Price of item multiplied by the amount of items ) 
                            console.log(totalEditSum);
                            localStorage.setItem('total_editted_single_sum', totalEditSum);
			}
		});
                  $(".basis-amount-blueprint").html(totalEditSum);
                  
            },
                                   
            /* Calculates the total sum of all chosen items (Edit blueprint) */
            this.editCalculateMonthlySumOfAll = function(){
               
                let totalEditMonthlySum = 0;
		$(".edit-blauwdruk-item:input").each(function (){
			if ($(this).val() !== "") { // Get the sum only if value is not empty 
                            totalEditMonthlySum += $(this).val() * $(this).data("monthly-price"); // Calculate price: (Price of item multiplied by the amount of items ) 
                            console.log(totalEditMonthlySum);
                            localStorage.setItem('total_editted_monthly_sum', totalEditMonthlySum);
			}
		});
                  $(".total-monthly-amount-blueprint").html(totalEditMonthlySum);
            }, 
                                       
            /* Calculates the total sum of all chosen items */
            this.calculateMonthlySumOfAll = function(){
                let sumMonthlyDefaultItems = localStorage.getItem('sum_monthly_default'); // Retrieve sum of default items
		let sumMonthlyAdditionalItems = localStorage.getItem('sum_monthly_additional'); // Retrieve sum of additional items
                let sumMonthlyModulesItems = localStorage.getItem('sum_monthly_modules'); // Retrieve sum of modules items
		let totalMonthlySum = parseInt(sumMonthlyAdditionalItems) + parseInt(sumMonthlyModulesItems); // Calculate total sum of default, additonal items & modules items
		localStorage.setItem('sum_monthly_total', totalMonthlySum); // Save the total sum to local storage
		$('.total-monthly-amount').html(totalMonthlySum); // Show total sum
            },        
            
            /* Adds chosen items to the overview screen */
            this.addToOverzicht = function(){
                
                /* Create empty object, ready to be used in loop below */
                let obj = {};
                /* Create combined JSON from each input */
		$('.blauwdruk-item').each(function (name,value) {

                        obj[$(this).attr('id')] = {
                                        'itemName':          this.name,
                                        'itemId':            $(this).attr('id'),
                                        'itemSinglePrice':   $(this).data('price'),
                                        'itemMonthlyPrice':  $(this).data('monthly-price'),
                                        'itemDefaultAmount': $(this).attr('value'),
                                        'itemMaxAmount':     $(this).attr('max'),
                                        'itemMinAmount':     $(this).attr('min'),
                                        'itemValue':         $(this).val()
                        };
		});
                
                // Save overzicht array in local storage
		localStorage.setItem('overzicht', JSON.stringify(obj));
                
                // Assign variable to "Overzicht" value in the local storage
		let overzicht = JSON.parse(localStorage.getItem('overzicht'));
                
                // Empty div before appending new items
		$('.overzicht-items').html(""); 
		
                // Add each chosen item to "Overzicht"
                $.each(overzicht, function (index, value) {
                    /* Only append selected items if or more selected */
                    if (value['itemValue'] >= "1") {
                        $('.overzicht-items').append("<label class='item-label'><div class='amount-overzicht'><br>" + value['itemValue'] + "</div>" + value['itemName'] + "</label><br>");
                    } 
		});
            },
                    
            /* Add to editted overzicht */
            this.addToEdittedOverzicht = function(){
                  
                let EdittedObj = {};
                  
                /* Create JSON from each input */
		$('.edit-blauwdruk-item').each(function(name,value) {
                    console.log(this.value);
                        EdittedObj[$(this).attr('id')] = {
                                        'itemName':          this.name,
                                        'itemId':            $(this).attr('id'),
                                        'itemSinglePrice':   $(this).data('price'),
                                        'itemMonthlyPrice':  $(this).data('monthly-price'),
                                        'itemDefaultAmount': $(this).attr('value'),
                                        'itemMaxAmount':     $(this).attr('max'),
                                        'itemMinAmount':     $(this).attr('min'),
                                        'itemValue':         $(this).val()
                        };
		});
                
                // Save overzicht array in local storage
		localStorage.setItem('editted_overzicht', JSON.stringify(EdittedObj)); 
            };
            
            /* Clear the localStorage data from previous uses */
            this.clearLocalStorage = function(){
                localStorage.removeItem("sum_additional");
                localStorage.removeItem("sum_modules");
                localStorage.setItem("sum_total", 0);
                localStorage.setItem("overzicht", "");
                localStorage.setItem("total_editted_single_sum", "");
            },
            
            /* Saves/Submits the configuration to database */        
            this.saveBlueprint = function(){   
                
                /* Only if customer name is not empty */
                if( $('#customer').val()!==""){

                    /* Only if user has confirmed */
                    if (confirm("Blauwdruk configuratie opslaan?")){

                        /* Get the customer name */
                        let customerName = $("#customer").val(); 

                        /* Retrieve the saved configuration from localStorage */
                        let savedConfiguration  = JSON.parse(localStorage.getItem("overzicht"));
                        let singlePaymentPrice  = JSON.parse(localStorage.getItem("sum_total"));
                        let monthlyPaymentPrice = JSON.parse(localStorage.getItem("sum_monthly_total"));

                        /* POST the saved configuration to PHP file that inserts data into DB */
                        let saveConf = $.post("controller.php", {
                                conf: savedConfiguration, // Saved configuration
                                totalPrice: singlePaymentPrice, // Total single price
                                monthlyTotalPrice: monthlyPaymentPrice, // Monthly total price
                                customer: customerName // Name of the customer
                        },function(data, status){
                                                           // If request has been succesful
                                if (status === 'success') { 
                                    console.info("Blauwdruk succesvol opgeslagen.");
                                }
                        });

                        /* If request is done and succeeded */
                        saveConf.done(function () {
                                $(".conf-option, .customer-wrapper, .save").fadeOut(); // Hide the page elements
                                $(".application-title").hide().html("De configuratie is opgeslagen.").fadeIn(); // Change title of application

                            /* Wait x seconds before redirecting to blueprints page */
                                setTimeout(function () {
                                        window.location.replace("blueprints.php");
                                }, 2500);
                        });

                        /* If requests fails / no network */
                        saveConf.fail(function () {
                                console.error("AJAX verzoek heeft gefaald of er is geen netwerkverbinding");
                        });
                        }
                    }else{
                        alert("Klantnaam is vereist.\n Voer eerst een klantnaam in");
                    }
            },
            
            /* Save the editted blueprint */
            this.saveEdittedBlueprint = function(){
                
                /* Only if user has confirmed */
                if (confirm("Blauwdruk wijziging opslaan?")){
                   
                    /* Retrieve the saved configuration from localStorage */
                    let edittedConfiguration       = JSON.parse(localStorage.getItem("editted_overzicht"));
                    let edittedSinglePaymentPrice  = JSON.parse(localStorage.getItem("total_editted_single_sum"));
                    let edittedMonthlyPaymentPrice = JSON.parse(localStorage.getItem("total_editted_monthly_sum"));
                    let edittedBpId                = $("#bp_id").val();            
                    
                    /* POST the saved configuration to PHP file that inserts data into DB */
                    let saveEdittedConf = $.post("controller.php", {
                            edittedConf:              edittedConfiguration,       // Editted configuration
                            edittedTotalPrice:        edittedSinglePaymentPrice,  // Total single price
                            edittedMonthlyTotalPrice: edittedMonthlyPaymentPrice, // Monthly total price
                            edittedBlueprintId:       edittedBpId
                    },function(data, status){
                            // If request has been succesful
                            if (status === 'success') { 
                                console.info("Wijziging van Blauwdruk succesvol opgeslagen.");
                            }
                    });
                    
                    /* If request is done and succeeded */
                    saveEdittedConf.done(function () {
                        
                        // Change title of application
                        $(".application-title").hide().html("De configuratie is opgeslagen.").fadeIn(); 
                           
                        /* Wait x seconds before redirecting to blueprints page */
                            setTimeout(function () {
                                    window.location.replace("blueprints.php");
                            }, 2500);
                    });
                    /* If requests fails / no network */
                    saveEdittedConf.fail(function () {
                            console.error("AJAX verzoek heeft gefaald of er is geen netwerkverbinding");
                    });
                }
            },     
                       
            /* Get all saved blueprints from DB */
            this.getAllSavedBlueprints = function(){
                
                let requestBlueprints = $.getJSON("controller.php?getBlueprints", function(){});
                requestBlueprints.done(function(data){
                     $.each(data, function(mainIndex, mainValue){
                         let blueprintData = mainValue.blueprint_data;
                         let creationDate  = mainValue.creation_date;
                         let userCustomer = mainValue.customer_name;
                     });
                    
                    console.info('Ophalen van blueprints gelukt (getAllSavedBlueprints)');
                });
                
                requestBlueprints.fail(function(){
                    console.error('Ophalen van blueprints mislukt (getAllSavedBlueprints)');
                });
            };
        };