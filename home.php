<?php
 if(!isset($_SESSION)){session_start();}
    
/* If login form submitted & password has value */
if(isset($_POST['submit'])){
    
    $email    = $_POST['email'];
    $password = $_POST['password'];
    
    /* Attempt to log the user in */
    require_once('class.login.php');
    $user = new Login();
    $user->logUserIn($email, $password); 
}
    
    /* If user is logged in */
    if($_SESSION['loggedIn'] === true){
       header("Location: index.php");
    }else{
        header("Location: login.php");
    }