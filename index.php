<?php

    /* If session not started yet, start it */
    if(!isset($_SESSION)){session_start();}
    
    /* Include and initialise the class */
    require_once "configurator.class.php";
    $configurator = new Configurator();

    /* If user is not logged in, redirect him back to login page */
    if($_SESSION['loggedIn'] !== true){
        header("Location: login.php");
    }
?>

<!DOCTYPE html>
<html lang="nl">
   <head>
       <!-- Include the page headers -->
      <?php require_once 'headers.php';?>
   </head>
   <body>
      <div id="conf-wrapper">
          
        <br>
         
        <!-- Logo S4Financials -->
        <?=$configurator->getLogo()?>
        
        <!-- Menu -->
        <?=$configurator->getAllMenuItems()?>
         
         <!-- Whitespaces -->
         <div class="whitespace-lg"></div> 
         <div class="whitespace-lg"></div>
         
         <div class="inner-wrapper">
           <div class="whitespace-sm"></div> 
           
           <!-- Application title -->
            <?=$configurator->getTitle()?>
                       
            <div class="whitespace-sm"></div> 
            <hr>
           <div class="whitespace-sm"></div> 

            <form method="POST" action="controller.php?saveconfig">
            <!-- "Basisinrichting" -->
            <div class="conf-option">
               <div class="titles basisinrichting">
                  <span class="number">1</span>
                  Basisinrichting
                  <img class="arrow-down" src="img/arrow-down.png" width="25">
               </div>
                 
               <div class="basisinrichting-items"> 
                  <?=$configurator->getAllDefaultItems();?>
               </div>
                
               <span class="total">
               Eenmalig :&nbsp;&euro;&nbsp;<span class="basis-amount"></span>
               </span>
            </div>
            
             <div class="whitespace-sm"></div>
             
             <!-- Aanvullende investeringen -->
            <div class="conf-option">
               <div class="titles aanvullend">
                  <span class="number">2</span>
                  Aanvullende investeringen
                  <img class="arrow-down" src="img/arrow-down.png" width="25">
               </div>
               <div class="aanvullend-items">
                  <?=$configurator->getAllAdditionalItems();?>
               </div>
               <span class="total">
                 Eenmalig :&nbsp;&euro;&nbsp;<span class="aanvullend-amount">0</span>
               </span>
               <span class="total-monthly">
               Maandelijks :&nbsp;&euro;&nbsp;<span class="aanvullend-monthly-amount">0</span>
               </span>
            </div>
             
              <div class="whitespace-sm"></div>
             
            <!-- Modules -->
            <div class="conf-option">
               <div class="titles modules">
                  <span class="number">3</span>
                  Modules
                  <img class="arrow-down" src="img/arrow-down.png" width="25">
               </div>
               <div class="modules-items">
                  <?=$configurator->getAllModuleItems();?>
               </div>
               <span class="total">
                Eenmalig :&nbsp;&euro;&nbsp;
               <span class="modules-amount">0</span>
               </span>
                 <span class="total-monthly">
               Maandelijks :&nbsp;&euro;&nbsp;<span class="modules-amount-monthly">0</span>
               </span>
            </div>
                </form>
             <div class="whitespace-sm"></div>
             
             <!-- Overzicht & subtotaal -->
            <div class="conf-option">
               <div class="titles overzicht">
                  <span class="number">4</span>
                  Overzicht & subtotaal
                    <img class="arrow-down" src="img/arrow-down.png" width="25">
               </div>
               <div class="overzicht-items">
               </div>
                
               <span class="total">
                Eenmalig :&nbsp;&euro;&nbsp;<span class="total-amount">0</span>
               </span>
                <input class="single-total" name="single-total" value="" type="hidden">
                 <span class="total-monthly">
               Maandelijks :&nbsp;&euro;&nbsp;<span class="total-monthly-amount">0</span>
               </span>
                <input class="monthly-total" name="monthly-total" value="" type="hidden">
            </div>

             <!-- Customer name --> 
           <div class="customer-wrapper">
           <label for="customer">Naam klant:</label>
           <br>
           <input id="customer" type="text" name="customer" required>
           </div>         
         </div>
          <div class="whitespace-sm"></div>
          <!-- Submit configuration to DB -->
         <button class="save" type="submit">Configuratie opslaan</button>
         <br>
      </div>
      
   </body>
</html>