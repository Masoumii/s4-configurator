<?php
    if(!isset($_SESSION)){session_start();}
    
    /* If user is not logged in, redirect him back to login page */
    if($_SESSION['loggedIn'] !== true){
        header("Location: login.php");
    }
    
    require_once 'configurator.class.php';
    $configurator = new Configurator();
?>

<!DOCTYPE html>
<html lang="nl">
   <head>
      <?php require_once 'headers.php';?>
   </head>
   <body>
      <div id="conf-wrapper">
        <br>
        
         <!-- Logo S4Financials -->
        <?=$configurator->getLogo()?>
         
        <!-- Menu -->
        <?=$configurator->getAllMenuItems()?>
        
         <!-- Whitespaces -->
        <div class="whitespace-lg"></div> 
        <div class="whitespace-sm"></div>
        
        <!-- Inner wrapper of page -->
        <div class="inner-wrapper">
            

             <div class="whitespace-sm"></div> 
            <div class="whitespace-md"></div> 
            
                     <!-- Application title -->
            <h1 class="application-title">Opgeslagen blauwdrukken (<?=$_SESSION['user_name']?>)</h1>
            
            <div class="whitespace-sm"></div> 
            <hr>
           <div class="whitespace-sm"></div> 
           <div class="whitespace-sm"></div> 
            
            <!-- Load and show all saved blueprints -->
            <?=$configurator->getAllSavedBlueprints()?>
           
        </div>
      </div>