<?php

    /* If session not started yet, start it */
    if(!isset($_SESSION)){session_start();}
    
    /* Include and initialise the class */
    require_once "configurator.class.php";
    $configurator = new Configurator();

    /* If user is not logged in, redirect him back to login page */
    if($_SESSION['loggedIn'] !== true){
        header("Location: login.php");
    }

?>

<!DOCTYPE html>
<html lang="nl">
   <head>
       <!-- Include the page headers -->
      <?php require_once 'headers.php';?>
   </head>
   <body>
      <div id="conf-wrapper">
        <br>
        <!-- Logo S4Financials -->
        <?=$configurator->getLogo()?>
        
        <!-- Menu -->
        <?=$configurator->getAllMenuItems()?>
         
         <!-- Whitespaces -->
         <div class="whitespace-lg"></div> 
         <div class="whitespace-lg"></div>
         
         <div class="inner-wrapper">
           <div class="whitespace-sm"></div> 
           
           <!-- Application title -->
           <h1>Blauwdruk</h1>
                       
            <div class="whitespace-sm"></div> 
            <hr>
           <div class="whitespace-sm"></div> 

             <!-- Overzicht & subtotaal -->
            <div class="conf-option">
               <div class="titles overzicht">
                  Opgeslagen blauwdruk
                    <img class="arrow-down" src="img/arrow-down.png" width="25">
               </div>
               <div class="overzicht-items">
                   <?=$configurator->getSavedBlueprint($_GET['bp_id'])?>
               </div>
                
                <!-- Eenmalige totaalbedrag -->     
               <span class="total">
               Eénmalig :&nbsp;€&nbsp;
               <span class="basis-amount-blueprint"><?=$configurator->singlePrice?></span>
               </span>
               
               <!-- Manadelijkse totaalbedrag -->
               <span class="total-monthly">
               Maandelijks :&nbsp;&euro;&nbsp;
               <span class="total-monthly-amount-blueprint"><?=$configurator->monthlyPrice?></span>
               </span>
                
                <input class="monthly-total" name="monthly-total" value="" type="hidden">
            </div>
             
             <!-- Customer name --> 
           <div class="customer-wrapper">
<!--           <label for="customer">Naam klant:</label>-->
           <br>
           <input value="" type="hidden" id="customer" name="customer" required>
           </div>         
         </div>
         
          <div class="whitespace-sm"></div>
          <!-- Submit configuration to DB -->
         <input id="bp_id" type="hidden" value="<?=$_GET['bp_id']?>">
         <button class="saveItemEdit" type="submit">Configuratie bijwerken</button>
         
         <br>
      </div>
   </body>
</html>