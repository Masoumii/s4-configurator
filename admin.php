<?php
 /* Start session if not started yet */
    if(!isset($_SESSION)){session_start();}
    /* If user is not logged in , redirect him back to login page */
    if($_SESSION['loggedIn'] !== true){
        header("Location: login.php");
    }
    /* If user is not admin, redirect him back to homepage */
    if($_SESSION['isAdmin'] !== '1'){
         header("Location: index.php");
    }
    require_once 'configurator.class.php';
    $configurator = new Configurator();
?>


<!DOCTYPE html>
<html lang="nl">
   <head>
      <?php require_once 'headers.php';?>
   </head>
   <body>
      <div id="conf-wrapper">
        <br>
        
         <!-- Logo S4Financials -->
        <?=$configurator->getLogo()?>
         
        <!-- Menu -->
        <?=$configurator->getAllMenuItems()?>
        
         <!-- Whitespaces -->
        <div class="whitespace-lg"></div> 
        <div class="whitespace-md"></div>
        
        <!-- Inner wrapper of page -->
        <div class="inner-wrapper">
            <div class="whitespace-sm"></div> 
            <div class="whitespace-md"></div> 
            
           <!-- Application title -->
            <h1 class="application-title">Configuratiebeheer</h1>
            
            <div class="whitespace-sm"></div> 
            <hr>
           <div class="whitespace-sm"></div> 
           <div class="whitespace-sm"></div> 
            <a class='createItem' href='create.php'><img width="20" src="img/add.png">&nbsp;&nbsp;Maak een nieuwe item aan</a><br>
         
            <h2>of <br><span style="font-size: 0.85em;">wijzig bestaande items:</span></h2><br>
            <?=$configurator->loadAdmin()?>
        </div>
      </div>