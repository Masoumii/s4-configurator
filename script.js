/* on page-load complete */
$(function () {

	/* Initialize the class */
	const configurator = new Configurator();

	/* On button ("SAVE") click, run function */
	$(".save").on("click", function () {
		configurator.saveBlueprint(); // Submit the saved configuration to database
	});
        /* Save the editted blueprint back into database */
        $('.saveItemEdit').on("click", function (){
                configurator.saveEdittedBlueprint();
        });

	/* Run functions on input(amount of items) change */
	$(".blauwdruk-item:input").on('keyup mouseup', function () {
             
		configurator.calculateSumOfAanvullend(); // Calculates the sum of all aanvullend
                configurator.calculateMonthlySumOfAanvullend(); // Calculates the monthly sum of all aanvullend
                
		configurator.calculateSumOfModules(); // Calculates sum of module items
                configurator.calculateMonthlySumOfModules(); // Calulates monthly sum of all modules
                
                configurator.calculateSumOfAll(); // Calculates sum of ALL items
                configurator.calculateMonthlySumOfAll(); // Calculates monthly sum of ALL items
                +
		configurator.addToOverzicht(); // Appends chosen items to "Overzicht"
	});
        
        /* Run functions on input(amount of items) change */
	$(".edit-blauwdruk-item").on('keyup mouseup', function () {
            configurator.editCalculateSumOfAll(); // Calculates sum of ALL items
            configurator.editCalculateMonthlySumOfAll(); // Calculates the monthly sum of ALL items
            configurator.addToEdittedOverzicht(); // Appends editted items to overzicht
	});

	/* 1: Registers clicks on "Basisinrichting" div */
	$(".basisinrichting").on("click", function () {
		$(".basisinrichting-items").slideToggle();
		$(".aanvullend-items").slideUp();
		$(".overzicht-items").slideUp();
		$(".modules-items").slideUp();
	});

	/* 2: Registers clicks on "Aanvullende" div */
	$(".aanvullend").on("click", function () {
		$(".aanvullend-items").slideToggle();
		$(".basisinrichting-items").slideUp();
		$(".overzicht-items").slideUp();
		$(".modules-items").slideUp();
	});

	/* 3: Registers clicks on "Modules" div */
	$(".modules").on("click", function () {
		$(".modules-items").slideToggle();
		$(".aanvullend-items").slideUp();
		$(".basisinrichting-items").slideUp();
		$(".overzicht-items").slideUp();
	});

	/* 4: Registers clicks on "Overzicht" div */
	$(".overzicht").on("click", function () {
		$(".overzicht-items").slideToggle();
		$(".basisinrichting-items").slideUp();
		$(".aanvullend-items").slideUp();
		$(".modules-items").slideUp();
	});
        
        /* 5: Registers clicks on Editted overzicht tab in "blueprint.php" */
        $(".opgeslagen-overzicht").on("click", function (){
            $(".opgeslagen-overzicht-items").slideToggle();
        });

	/* Run methods on page-load: Calculate sum of items */
	configurator.calculateSumOfBasis();
	configurator.calculateSumOfAanvullend();
	configurator.calculateSumOfModules();
	configurator.calculateSumOfAll();
        configurator.editCalculateSumOfAll();
        
        /* Run methods on page-load: Calculate monthly sum of items */
        configurator.calculateMonthlySumOfAanvullend();
        configurator.calculateMonthlySumOfModules();
        configurator.calculateMonthlySumOfAll();
        configurator.editCalculateMonthlySumOfAll();

	/* Run method on page-load: Clear localStorage from last use */
	configurator.clearLocalStorage();
        
        /* Hide certain input fields based on select option choice */
          $('#itemCategory').change(function(){
              $('.itemMonthlyPrice').val("");
              /* if category is not basisinrichting */  
            if($('#itemCategory').val()!=="1"){
                $('.monthly').show(); 
                  /* If category is basisinrichting */
            }else{
                 $('.monthly').hide();
            }
          });
});