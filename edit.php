<?php

 /* Start session if not started yet */
    if(!isset($_SESSION)){session_start();}
    /* If user is not logged in , redirect him back to login page */
    if($_SESSION['loggedIn'] !== true){
        header("Location: login.php");
    }
    /* If user is not admin, redirect him back to homepage */
    if($_SESSION['isAdmin'] !== '1'){
         header("Location: index.php");
    }

    /* Edit configuration items */
    if(isset($_GET['item_id'])){
        $itemID = $_GET['item_id']; 
        require_once('configurator.class.php');
        $configurator = new Configurator();  
    }
    ?>


<!DOCTYPE html>
<html lang="nl">
   <head>
       <!-- Include the page headers -->
      <?php require_once 'headers.php';?>
   </head>
   <body>
       <div id="conf-wrapper">
               
        <br>
         
        <!-- Logo S4Financials -->
        <?=$configurator->getLogo()?>
        
        <!-- Menu -->
        <?=$configurator->getAllMenuItems()?>
         
         <!-- Whitespaces -->
         <div class="whitespace-lg"></div> 
         <div class="whitespace-md"></div> 
         <div class="whitespace-sm"></div>
         
         <div class="inner-wrapper">
           <div class="whitespace-sm"></div> 
           
           <!-- Application title -->
            <h1 class="application-title">Wijzig item gegevens (<?=$_SESSION['user_name']?>)</h1>
            
            <!-- "items" -->
            <?=$configurator->editItem($itemID)?>
       </div>
       </div>
   </body>