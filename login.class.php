<?php
/* Start session if not already started */
 if(!isset($_SESSION)){session_start();}
 
/* Class */
class Login
{
    /* Private variables */
    private $id         = null;
    private $email      = null;
    private $inputPass  = null;
    private $dbPass     = null;
    private $isAdmin    = false;
    private $login      = false;
    
    /* Constructor */
    public function __construct(){}
    
    /* Login method */
    public function logUserIn($email, $pass){
        
        $_SESSION['loggedIn'] = false; // Set session
        $this->email     = $email;
        $this->inputPass = $pass;  // Password from user input
        
        require_once("dbconfig.php");
        $conn = DatabaseConnection::getConnection();
        $q    = "SELECT * FROM conf_users WHERE email = '$this->email'";
        $stmt = $conn->prepare($q);
        $stmt->execute();
        
        while ($row = $stmt->fetch()) {
            
            /* Assign DB pass to the class variable */
            $this->dbPass = $row['pass'];

            /* Password-check: Compare user pass with DB pass */
            if ($this->inputPass === $this->dbPass){
                
                $_SESSION['loggedIn']  = true; // Set session
                $_SESSION['user_id']   = $row['id']; // Save ID in session for later use
                $_SESSION['user_name'] = $row['name']; // Save the username in Session to show to user
                $_SESSION['isAdmin']   = $row['isAdmin']; // Save isAdmin value in sessio  
            }  
        }   
    }
    
    /* Get login value: logged in or not */
    public function getLogin()
    {
        return $this->login;
    }
    
    /* Get USER ID */
    public function getId()
    {
        return $this->id;
    }
    
    /* Log out method */
    public function logUserOut()
    {
        session_start(); 
        session_destroy(); 
        header("location: login.php");
    }
    
}