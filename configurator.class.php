<?php
/* Class */
class Configurator
{

    /* Private variables */
    private $blueprint           = null;
    private $blueprintId         = null;
    private $decodedBlueprint    = null;
    private $blueprintDate       = null;
    private $userId              = null;
    private $itemId              = null;
    private $itemActive          = null;
    private $itemName            = null;
    private $itemSinglePrice     = null;
    private $itemMonthlyPrice    = null;
    private $itemDefaultAmount   = null;
    private $itemThresholdAmount = null;
    private $itemCategory        = null;
    private $itemCssClass        = null;
    private $itemPrice           = null;
    private $itemMinAmount       = null;
    private $itemMaxAmount       = null;
    private $itemCreationDate    = null;
    private $itemDescription     = null;
    private $itemPriority        = null;
    private $totalPrice          = null;
    private $totalMonthlyPrice   = null;
    private $monthlyItemPrice    = null;
    private $customerName        = null;
    private $defaultItems        = null;
    private $additionalItems     = null;
    private $moduleItems         = null;
    private $isAdmin             = 0;
    private $resultsArray        = [];
    private $finalResults        = [];

    /* Constructor */
    public function __construct()
    {
    }
    
    /* Get the ID of item */
    public function getItemId(){
        return $this->itemId;
    }
    
    /* Set the ID of item */
    public function setItemId($itemID){
        $this->itemId = $itemID;
    }

    /* Get the name of item */
    public function getItemName()
    {
        return $this->itemName;
    }
    
    /* Set the name of item */
    public function setItemName($itemName)
    {
        $this->itemName = $itemName;
    }
    
    /* Get the CSS-class of item */
    public function getItemCssClass()
    {
        return $this->itemCssClass;
    }
    
    /* Set the CSS-class of item */
    public function setItemCssClass($cssClass)
    {
        $this->itemCssClass = $cssClass;
    }
    
    /* return the price of item */
    public function getItemPrice()
    {
        return $this->itemPrice;
    }
    
    /* get the price of item by ID */
    public function getItemPriceById($blueprintId){
        $this->blueprintId = $blueprintId;
        require_once "dbconfig.php";
        $conn = DatabaseConnection::getConnection();
        $q = "SELECT single_price FROM conf_blueprints WHERE user_id ='$this->blueprintId'";
        $stmt = $conn->prepare($q);
        $stmt->execute();
        
         while ($row = $stmt->fetch())
        {
             $this->itemPrice = $row['single_price'];
             return $this->itemPrice;
        }
        
    }
    
    /* Set the price of item */
    public function setItemPrice($itemPrice)
    {
        $this->itemPrice = $itemPrice;
    }
    
    /* Get the monthly price of the item */
    public function getMonthlyItemPrice()
    {
        return $this->monthlyItemPrice;
    }
    
    /* Set the monthly price of the item */
    public function setMonthlyItemPrice($monthlyItemPrice)
    {
        $this->monthlyItemPrice = $monthlyItemPrice;
    }
    
    /* Get the max amount of item */
    public function getItemMaxAmount()
    {
        return $this->itemMaxAmount;
    } 
    
    /* Set the max amount of item */
    public function setItemMaxAmount($itemMaxAmount)
    {
        $this->itemMaxAmount = $itemMaxAmount;
    }
    
    /* Get the min amount of item */
    public function getItemMinAmount(){
         return $this->itemMinAmount;
    }
    
    /* Set the min amount of item */
    public function setItemMinAmount($itemMinAmount)
    {
        $this->itemMinAmount = $itemMinAmount;
    }
    
    /* Get the item creation date */
    public function getItemCreationDate()
    {
        return $this->itemCreationDate;
    }
    
    /* Set the item creation date */
    public function setItemCreationDate($itemCreationDate)
    {
        $this->itemCreationDate = $itemCreationDate;
    }
    
    /* Get the description of item */
    public function getItemDescription()
    {
        return $this->itemDescription;
    }
    
    /* Set the description of item */
    public function setItemDescription($itemDescription)
    {
        $this->itemDescription = $itemDescription;
    }

    /* Get all default items */
    public function getAllDefaultItems()
    {
        /* Connect to DB and get items */
        require_once "dbconfig.php";
        $conn = DatabaseConnection::getConnection();
        $q = "SELECT * FROM conf_item WHERE active=1 AND categorie_id=1 ORDER BY priority ASC";
        $stmt = $conn->prepare($q);
        $stmt->execute();
        ?>

<?php  
        /* Bind items from DB to class variables */
        while ($row = $stmt->fetch())
        {
            $this->setItemId($row['id']);
            $this->setItemName($row['item']);
            $this->setItemDescription($row['description']);
            $this->setItemPrice($row['single_price']);
            $this->setMonthlyItemPrice($row['monthly_price']);
            $this->setItemCssClass($row['css_class']);
            $this->setItemCreationDate($row['create_datetime']);
        ?>
            <!-- Generates the HTML while in the loop --> 
            <label for="<?=$this->getItemId()?>" title="<?=$this->getItemDescription()?>" class="item-label">
                -&nbsp;<?=$this->getItemName()?>
                <input min="1" max="1" id="<?=$this->getItemId()?>" name="<?=$this->getItemName()?>" data-price="<?=$this->getItemPrice()?>" data-monthly-price="<?=$this->getMonthlyItemPrice?>" class="basis-cb blauwdruk-item <?=$this->getItemCssClass()?>" name="item" type="number" value="1" >
                </label>
                <br>
                <span class="productPrice">&euro;&nbsp;<?=round($this->getItemPrice())?></span>
                <br>
                <?php
                }
                ?>  
                <?php
            }
            
    /* Get all additional items */
    public function getAllAdditionalItems()
    {

        /* Connect to DB and get items */
        require_once "dbconfig.php";
        $conn = DatabaseConnection::getConnection();
        $q = "SELECT * FROM conf_item WHERE categorie_id = 2 AND active = 1";
        $stmt = $conn->prepare($q);
        $stmt->execute();
        
        /* Bind items from DB to class variables */
        while ($row = $stmt->fetch())
        {
            $this->setItemId($row['id']);
            $this->setItemName($row['item']);
            $this->setItemDescription($row['description']);
            $this->setItemPrice($row['single_price']);
            $this->setMonthlyItemPrice($row['monthly_price']);
            $this->setItemCssClass($row['css_class']);
            $this->setItemCreationDate($row['create_datetime']);
            $this->setItemMinAmount($row['default_amount']);
            $this->setItemMaxAmount($row['max_amount']);
            
            //$json_value=["General"=>["name"=>"test"],"Modules"=>['6'=>1]];
        ?>          
            <!-- Generates the HTML while in the loop -->    
            <label title="<?=$this->getItemDescription()?>" class="item-label">
                - <?=$this->getItemName()?>
                <input id="<?=$this->getItemId()?>" name="<?=$this->getItemName()?>" data-monthly-price="<?=$this->getMonthlyItemPrice()?>" data-price="<?=$this->getItemPrice()?>" class="<?=$this->getItemCssClass()?> aanvullend-cb blauwdruk-item" min="<?=$this->getItemMinAmount()?>" max="<?=$this->getItemMaxAmount()?>" value="<?=$this->getItemMinAmount()?>" type="number" required>
            </label>
                <br>
                <span class="aanvullendeProductPrice">&euro;&nbsp;<?=round($this->getItemPrice()) ?></span>
                <br>
        <?php
        } 
    }
    
    
    /* Get all Module items */
    public function getAllModuleItems()
    {
        /* Connect to DB and get items */
        require_once "dbconfig.php";
        $conn = DatabaseConnection::getConnection();
        $q = "SELECT * FROM conf_item WHERE categorie_id = 3 AND active = 1";
        $stmt = $conn->prepare($q);
        $stmt->execute();
        
        /* Bind items from DB to class variables */
        while ($row = $stmt->fetch())
        {
            $this->setItemId($row['id']);
            $this->setItemName($row['item']);
            $this->setItemDescription($row['description']);
            $this->setItemPrice($row['single_price']);
            $this->setMonthlyItemPrice($row['monthly_price']);
            $this->setItemCssClass($row['css_class']);
            $this->setItemCreationDate($row['create_datetime']);
            $this->setItemMinAmount($row['default_amount']);
            $this->setItemMaxAmount($row['max_amount']);
        ?>          
            <!-- Generates the HTML while in the loop -->    
            <label title="<?=$this->getItemDescription()?>" class="item-label">
                - <?=$this->getItemName()?>
                <input id="<?=$this->getItemId()?>" name="<?=$this->getItemName()?>" data-monthly-price="<?=$this->getMonthlyItemPrice()?>" data-price="<?=$this->getItemPrice()?>" class="<?=$this->getItemCssClass()?> modules-cb blauwdruk-item" min="0" max="<?=$this->getItemMaxAmount()?>" value="<?=$this->getItemMinAmount()?>" type="number" required>
            </label>
                <br>
                <span class="modulesProductPrice">&euro;&nbsp;<?=round($this->getItemPrice())?></span>
                <br>
        <?php
        }
    }
    
    /* function to get a saved blueprint from DB with its ID */
    public function getSavedBlueprint($blueprintId){
        
        $this->userId    = $_SESSION['user_id'];
        $this->blueprintId = $blueprintId;
        require_once "dbconfig.php";
        
        $conn = DatabaseConnection::getConnection();
        $q = "SELECT * FROM conf_blueprints WHERE user_id = '$this->userId' AND blueprint_id = '$this->blueprintId'"; // Get all blueprints of the logged in user
        $stmt = $conn->prepare($q);
        $stmt->execute();
        ?>
        <?php
        while ($row = $stmt->fetch())
        {
            ?> 
            <?php
            $this->blueprintId    = $row['blueprint_id'];
            $this->blueprint      = json_decode($row['blueprint_data'], true);
            $this->singlePrice    = $row['single_price'];
            $this->monthlyPrice   = $row['monthly_price'];
            $this->customerName   = $row['customer'];
            $this->blueprintDate  = $row['create_datetime'];  
            ?>
                   <!-- Application title -->
            <h2 class="application-title">Blauwdruk (<?=$this->customerName?>)</h2>
            <?php

            foreach ($this->blueprint as $key => $value) {
                
                    ?>
                         <label title="<?=$this->getItemDescription()?>" for='<?=$value['itemId']?>' class='item-label'>
                         - <?=$value['itemName']?>
                         <input id="<?=$value['itemId']?>" name="<?=$value['itemName']?>" data-monthly-price="<?=$value['itemMonthlyPrice']?>" data-price="<?=$value['itemSinglePrice']?>" class="edit-blauwdruk-item" min="<?=$value['itemMinAmount']?>" max="<?=$value['itemMaxAmount']?>" value="<?=$value['itemValue']?>" type="number">
                         </label><br><br>  
                    <?php
            }    
        }?>
        <?php        
    }
    
    /* Function to get all the saved blueprints*/
    public function getAllSavedBlueprints()
    {
        // Get user ID from session
        $this->userId = $_SESSION['user_id']; 
        
        require_once 'dbconfig.php'; // Require the database connection configuration
        $conn = DatabaseConnection::getConnection();
        $q = "SELECT * FROM conf_blueprints WHERE user_id = '$this->userId' ORDER BY create_datetime DESC"; // Get all blueprints of the logged in user
        $stmt = $conn->prepare($q);
        $stmt->execute();
        
        /* Bind items from DB to class variables */
        while ($row = $stmt->fetch()){
            
            $this->blueprintId      = $row['blueprint_id']; // ID of blueprint
            $this->blueprint        = $row['blueprint_data']; // Blueprint JSON
            $this->itemCreationDate = $row['create_datetime']; // Creation date/time
            $this->customerName     = $row['customer']; // Name of the customer
            $this->singlePrice      = $row['single_price']; // Get the single price of customer
            $this->monthlyPrice     = $row['monthly_price']; // Get the monthly price of customer
            $this->decodedBlueprint = json_decode($this->blueprint, true); // Decoded JSON blueprint (associative array)
            ?>
                
                <div class="conf-option">
                <a href="blueprint.php?bp_id=<?=$this->blueprintId?>"> 
               <div class="blueprint-titles blauwdruk">
                   <span class="blueprint-customer-name"><?=$this->customerName?></span> 
                   <span class="blueprint-date"><?=date('d-m-Y', strtotime($this->itemCreationDate))?></span>
                
               </div>
                </a>
                    
               <div class="blauwdruk-items">     
                <?php   
                ?> 
               </div>
                    
               <!-- Eenmalige totaalbedrag -->     
               <span class="total">
               Eenmalig :&nbsp;€&nbsp;
               <span class=""><?=$this->singlePrice?></span>
               </span>
               
               <!-- Manadelijkse totaalbedrag -->
               <span class="total-monthly">
               Maandelijks :&nbsp;&euro;&nbsp;
               <span class=""><?=$this->monthlyPrice?></span>
               </span>
                    
            </div>
             <div class="whitespace-sm"></div>
            <?php
        }             
    }
    
    /* Get Admin Area */
    public function loadAdmin(){

        require_once 'dbconfig.php';
        $conn = DatabaseConnection::getConnection();
        $q = "SELECT * FROM conf_item ORDER BY categorie_id ASC";
        $stmt = $conn->prepare($q);
        $stmt->execute();
        
        while ($row = $stmt->fetch()){
            
            /* If item is set to inactive */
            if($row['active']==='0'){
                $activeString = "&nbsp;&nbsp;(Inactief)";
                ?>
                <a class="" href="edit.php?item_id=<?=$row['id']?>">
                <div class="conf-option">
                    <div class="titles inactive">
                     <?=$row['item']?><?=$activeString?>
                    </div>
                </div>
                </a>
                <br>
             <?php
            }else{
                ?>
                 <a class="" href="edit.php?item_id=<?=$row['id']?>">
                <div class="conf-option">
                    <div class="titles">
                        <?=$row['item']?>
                    </div>
                </div>
                </a>
                <br>
            <?php
            }
            ?> 
            <?php
        } 
    }
    
    /* Admin Area: Edit Item */
    public function editItem($itemID){
        
        $this->setItemId($itemID);
        
        require_once 'dbconfig.php';
        $conn = DatabaseConnection::getConnection();
        $q = "SELECT * FROM conf_item WHERE id = '$this->itemId'";
        $stmt = $conn->prepare($q);
        $stmt->execute();
        
        while ($row = $stmt->fetch()){
            ?>
                <div class="conf-option">
                <form method='POST' action='controller.php?saveEdit&id=<?=$row['id']?>'>
                    Actief<br>
                <select name='itemActive'>
                    <option value='1' <?php if($row['active'] === "1"): ?> selected="selected"<?php endif; ?>>Ja</option>
                    <option value='0' <?php if($row['active'] === "0"): ?> selected="selected"<?php endif; ?>>Nee</option>
                </select>
                <br><br>
                Categorie<br>
                <select id='itemCategory' name='itemCategory'>
                    <option value='1' <?php if($row['categorie_id'] === "1"): ?> selected="selected"<?php endif;?>>Basisinrichting</option>
                    <option value='2' <?php if($row['categorie_id'] === "2"): ?> selected="selected"<?php endif;?>>Aanvullende investeringen</option>
                    <option value='3' <?php if($row['categorie_id'] === "3"): ?> selected="selected"<?php endif;?>>Modules</option>
                </select>
                <br><br>
                Naam<br>
                <input name='itemName' class='input-edit text-edit' type='text' value='<?=$row['item']?>' required><br><br>
                Beschrijving<br>
                <input name='itemDescription' class='input-edit text-edit' type='text' value='<?=$row['description']?>'><br><br>
                Eénmalige kosten
                <input name='itemSinglePrice' class='input-edit' type='number' value='<?=$row['single_price']?>' required><br><br>
               
                <span class='monthly'>
                Maandelijkse kosten
                <input name='itemMonthlyPrice' class='input-edit itemMonthlyPrice' type='number' value='<?=$row['monthly_price']?>'><br><br>
                 </span>
                
                Vanaf hoeveelheid
                <input name='itemThresholdAmount' class='input-edit' type='number' value='<?=$row['threshold_amount']?>' required><br><br>
                Max. hoeveelheid
                <input name='itemMaxAmount' class='input-edit' type='number' value='<?=$row['max_amount']?>' required><br><br>
                Prioriteit in de lijst
                <input name='itemPriority' class='input-edit' type='number' value='<?=$row['priority']?>'><br><br>
                </div><br>
                <input style='bottom:0' class='saveEdit' type='submit' value='Bijwerken' name='saveEdit'>
                </form>
        <?php
        }
    }
    
    
    
    /* Get menu items */
    public function getAllMenuItems()
    {
        $isAdmin = $_SESSION['isAdmin'];
        
        if($isAdmin){
            ?>
            <!-- Menu buttons: Administrators -->
            <a class="adminArea" href="admin.php">Configuratiebeheer</a>
            <a class="savedConf" href="blueprints.php">Mijn configuraties</a>
            <a class="logout" href="controller.php?logout">Uitloggen</a>
            <?php
        }else{
            ?>
             <!-- Menu buttons: Users -->
            <a class="savedConf" href="blueprints.php">Mijn configuraties</a>
            <a class="logout" href="controller.php?logout">Uitloggen</a>
            <?php           
        }
    }
    
    /* Get application title */
    public function getTitle()
    {   ?>
        <h1 class="application-title">Blauwdruk Configurator (<?=$_SESSION['user_name']?>)</h1>
    <?php }
    
    /* Get application logo */
    public function getLogo()
    { ?>
        <a href="index.php"><img class="conf-logo" width="110" src="img/logo.png"></a>
    <?php
    }
    
    
    
    /* Save/submit the blueprint in Database */
    public function save($blueprint, $totalPrice, $monthlyTotalPrice, $userId, $customer)
    {
        $this->blueprint = $blueprint;
        $this->totalPrice = $totalPrice;
        $this->monthlyTotalPrice = $monthlyTotalPrice;
        $this->userId = $userId;
        $this->customerName = $customer;
        
        require_once('dbconfig.php');
        $conn = DatabaseConnection::getConnection();
        $q = "INSERT INTO conf_blueprints(user_id,blueprint_data,single_price,monthly_price,customer) VALUES('$this->userId','$this->blueprint', '$this->totalPrice', '$this->monthlyTotalPrice', '$this->customerName')";
        $stmt = $conn->prepare($q);
        $stmt->execute();
    }
    
    /* Save/submit the blueprint in Database */
    public function editSave($blueprint, $totalPrice, $monthlyTotalPrice, $blueprintID)
    {
        $this->blueprint = $blueprint;
        $this->totalPrice = $totalPrice;
        $this->monthlyTotalPrice = $monthlyTotalPrice;
        $this->blueprintId = $blueprintID;
        
        require_once('dbconfig.php');
        $conn = DatabaseConnection::getConnection();
        $q = "UPDATE conf_blueprints SET blueprint_data='$this->blueprint', single_price='$this->totalPrice', monthly_price='$this->monthlyTotalPrice' WHERE blueprint_id='$this->blueprintId'";
        $stmt = $conn->prepare($q);
        $stmt->execute();
    }
    
    /* Save editted items in Database */
    public function saveEdit(
            $itemId,
            $itemName,
            $itemDescription,
            $itemCategory,
            $itemActive,
            $itemSinglePrice,
            $itemMonthlyPrice,
            $itemThresholdAmount,
            $itemMaxAmount,
            $itemPriority
            ){
        
        $this->itemId = $itemId;
        $this->itemName = $itemName;
        $this->itemDescription = $itemDescription;
        $this->itemCategory = $itemCategory;
        $this->itemActive = $itemActive;
        $this->itemSinglePrice = $itemSinglePrice;
        $this->itemMonthlyPrice = $itemMonthlyPrice;
        $this->itemThresholdAmount = $itemThresholdAmount;
        $this->itemMaxAmount = $itemMaxAmount;
        $this->itemPriority = $itemPriority;
        
        require_once('dbconfig.php');
        $conn = DatabaseConnection::getConnection();
        $q = "UPDATE conf_item"
                . " SET item='$this->itemName',"
                . " categorie_id='$this->itemCategory',"
                . " single_price='$this->itemSinglePrice',"
                . " monthly_price='$this->itemMonthlyPrice',"
                . " threshold_amount='$this->itemThresholdAmount',"
                . " max_amount='$this->itemMaxAmount',"
                . " active='$this->itemActive',"
                . " description='$this->itemDescription',"
                . " priority='$this->itemPriority'"
                . " WHERE id='$this->itemId'";       
        $stmt = $conn->prepare($q);
        
        if( $stmt->execute()){
            header('Location: admin.php');
        }  
    }
    
    /* Create new items and save to database */
    public function createItem($itemName,$itemDescription,$itemCategory,$itemActive,$itemSinglePrice,$itemMonthlyPrice,$itemThresholdAmount,$itemMaxAmount,$itemPriority){
        $this->itemName = $itemName;
        $this->itemDescription = $itemDescription;
        $this->itemCategory = $itemCategory;
        $this->itemActive = $itemActive;
        $this->itemSinglePrice = $itemSinglePrice;
        $this->itemMonthlyPrice = $itemMonthlyPrice;
        $this->itemThresholdAmount = $itemThresholdAmount;
        $this->itemMaxAmount = $itemMaxAmount;
        $this->itemPriority = $itemPriority;
        
        require_once('dbconfig.php');
        $conn = DatabaseConnection::getConnection();
        
        $q = "INSERT INTO conf_item("
                . "categorie_id"
                . ",item,"
                . "single_price,"
                . "monthly_price,"
                . "threshold_amount,"
                . "max_amount,"
                . "active,"
                . "description,"
                . "priority)"
                . " VALUES ("
                . "'$this->itemCategory',"
                . "'$this->itemName',"
                . "'$this->itemSinglePrice',"
                . "'$this->itemMonthlyPrice',"
                . "'$this->itemThresholdAmount',"
                . "'$this->itemMaxAmount',"
                . "'$this->itemActive',"
                . "'$this->itemDescription',"
                . "'$this->itemPriority'"
                . ")";    
        
        $stmt = $conn->prepare($q);
        if( $stmt->execute()){
            header('Location: admin.php');
        }     
    }
}