<?php
 if(!isset($_SESSION)){session_start();}

/* If login form submitted & password has value */
if(isset($_POST['submit'])){
    
    $email    = $_POST['email'];
    $password = $_POST['password'];
    
    /* Attempt to log the user in with the user input */
    require_once('login.class.php');
    $user = new Login();
    $user->logUserIn($email, $password);
}
    
    /* If user is logged in */
    if($_SESSION['loggedIn'] === true){
       header("Location: index.php");
    }
?>


<!DOCTYPE html>
<html lang="nl">
<head>
<?php require_once 'headers.php'; ?>
</head>

    <body>
         <div id="conf-wrapper">
              <br>
             <img class="conf-logo" width="110" src="img/logo.png">
               <div class="whitespace-lg"></div> 

        <div class="inner-wrapper">
              <div class="whitespace-sm"></div> 
        <h1 class="application-title">Blauwdruk Configurator</h1>   
        <div class="conf-option"> 
        <form action="" method="POST">
            <input name="email" type="text" placeholder="Email" required autofocus><br>
            <input name="password" type="password" placeholder="Wachtwoord" required>
              <div class="whitespace-sm"></div> 
            <input class="login" name="submit" value="Inloggen" type="submit">
        </form>
        </div>
        </div>
         </div>
            
    </body>
</html>

