<?php

 /* Start session if not already started */
  if(!isset($_SESSION)){session_start();}

    /* Handle POST to database */
    if(isset($_POST['conf'])){
       $conf = json_encode($_POST['conf']);
       $totalPrice = $_POST['totalPrice'];
       $monthlyTotalPrice = $_POST['monthlyTotalPrice'];
       $customer = $_POST['customer'];
       $userID = $_SESSION['user_id'];
       require_once('configurator.class.php');
       $configurator = new Configurator();
       $configurator->save($conf, $totalPrice, $monthlyTotalPrice, $userID, $customer);
    }
    
    /* Saved editted blueprint to database */
    if(isset($_POST['edittedConf'])){
       $conf = json_encode($_POST['edittedConf']);
       $totalPrice = $_POST['edittedTotalPrice'];
       $monthlyTotalPrice = $_POST['edittedMonthlyTotalPrice'];
       $blueprintID = $_POST['edittedBlueprintId'];
       
       require_once('configurator.class.php');
       $configurator = new Configurator();
       $configurator->editSave($conf, $totalPrice, $monthlyTotalPrice, $blueprintID);
    }

    /* Handle logging out */
    if(isset($_GET['logout'])){
        require_once('login.class.php');
        $login = new Login();
        $login->logUserOut();
    }
    
    if(isset($_GET["saveconfig"]))
    {
         require_once('login.class.php');
         $configurator = new Configurator();
         $configurator->save();
    }
    
    
    /* Get all saved blueprints from DB */
    if(isset($_GET['getBlueprints'])){
       require_once('configurator.class.php');
       $configurator = new Configurator();
       $configurator->getAllSavedBlueprints();
    }
    
    /* Save editted items */
    if(isset($_POST['saveEdit'])){
        
        $itemId = $_GET['id'];
        $itemName = $_POST['itemName'];
        $itemDescription = $_POST['itemDescription'];
        $itemCategory = $_POST['itemCategory'];
        $itemActive = $_POST['itemActive'];
        $itemSinglePrice = $_POST['itemSinglePrice'];
        $itemMonthlyPrice = $_POST['itemMonthlyPrice'];
        $itemThresholdAmount = $_POST['itemThresholdAmount'];
        $itemMaxAmount = $_POST['itemMaxAmount'];
        $itemPriority = $_POST['itemPriority'];
        
        require_once('configurator.class.php');
        $configurator = new Configurator();
        $configurator->saveEdit(
                $itemId,
                $itemName,
                $itemDescription,
                $itemCategory,
                $itemActive,
                $itemSinglePrice,
                $itemMonthlyPrice,
                $itemThresholdAmount,
                $itemMaxAmount,
                $itemPriority);
    }
    
    /* Create new item */
    if(isset($_GET['createItem'])){
        $itemName = $_POST['itemName'];
        $itemDescription = $_POST['itemDescription'];
        $itemCategory = $_POST['itemCategory'];
        $itemActive = $_POST['itemActive'];
        $itemSinglePrice = $_POST['itemSinglePrice'];
        $itemMonthlyPrice = $_POST['itemMonthlyPrice'];
        $itemDefaultAmount = $_POST['itemDefaultAmount'];
        $itemThresholdAmount = $_POST['itemThresholdAmount'];
        $itemMaxAmount = $_POST['itemMaxAmount'];
        $itemPriority = $_POST['itemPriority'];
        
        require_once('configurator.class.php');
        $configurator = new Configurator();
        $configurator->createItem(
                $itemName,
                $itemDescription,
                $itemCategory,
                $itemActive,
                $itemSinglePrice,
                $itemMonthlyPrice,
                $itemThresholdAmount,
                $itemMaxAmount,
                $itemPriority);
    }
