<?php
 /* Start session if not started yet */
    if(!isset($_SESSION)){session_start();}
    /* If user is not logged in , redirect him back to login page */
    if($_SESSION['loggedIn'] !== true){
        header("Location: login.php");
    }
    /* If user is not admin, redirect him back to homepage */
    if($_SESSION['isAdmin'] !== '1'){
         header("Location: index.php");
    }
    require_once 'configurator.class.php';
    $configurator = new Configurator();
?>


<!DOCTYPE html>
<html lang="nl">
   <head>
      <?php require_once 'headers.php';?>
   </head>
   <body>
      <div id="conf-wrapper">
        <br>
        
         <!-- Logo S4Financials -->
        <?=$configurator->getLogo()?>
         
        <!-- Menu -->
        <?=$configurator->getAllMenuItems()?>
        
 <!-- Whitespaces -->
        <div class="whitespace-lg"></div> 
        <div class="whitespace-md"></div>
        
        <!-- Inner wrapper of page -->
        <div class="inner-wrapper">
            <div class="whitespace-sm"></div> 
            <div class="whitespace-md"></div> 
            
           <!-- Application title -->
            <h1 class="application-title">Nieuwe item aanmaken</h1>
            
            <div class="whitespace-sm"></div> 
            <hr>
           <div class="whitespace-sm"></div> 
           <div class="whitespace-sm"></div> 
             <div class="conf-option">
                <form method='POST' action='controller.php?createItem'>
                    Actief<br>
                <select name='itemActive'>
                    <option value='1'>Ja</option>
                    <option value='0'>Nee</option>
                </select>
                <br><br>
                Categorie<br>
                <select id='itemCategory' name='itemCategory'>
                    <option value='1'>Basisinrichting</option>
                    <option value='2'>Aanvullende investeringen</option>
                    <option value='3'>Modules</option>
                </select>
                <br><br>
               
                Naam<br>
                <input name='itemName' class='input-edit text-edit' type='text' value='' required><br><br>
                Beschrijving<br>
                <input name='itemDescription' class='input-edit text-edit' type='text' value=''><br><br>
               
                Eénmalige kosten
                <input name='itemSinglePrice' class='input-edit ' type='number' value='' required><br><br>
                
                <span style='display:none' class='monthly'>
                Maandelijkse kosten
                <input name='itemMonthlyPrice' class='input-edit itemMonthlyPrice' value='' type='number'><br><br>
                </span>
               
                <!--<span class='hideOption'>
                Standaard hoeveelheid:
                <input id='itemDefaultAmount' name='itemDefaultAmount' class='input-edit' type='number' value=''><br><br>
                </span>
                -->
                
                Vanaf hoeveelheid
                <input name='itemThresholdAmount' class='input-edit' type='number' value='' required><br><br>
                Max. hoeveelheid
                <input name='itemMaxAmount' class='input-edit' type='number' value='' required><br><br>
                
                <span class='hideOption'>
                Prioriteit in de lijst
                <input name='itemPriority' class='input-edit' type='number' value=''><br><br>
                 </span>
                
                </div><br>
                <input style='bottom:0' class='saveEdit' type='submit' value='Item aanmaken' name='createItem'>
                </form>
                 
            <br>
            <br>
        </div>
      </div>